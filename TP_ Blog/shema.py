from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
from sqlalchemy.orm import declarative_base, relationship

# Définir la base déclarative
Base = declarative_base()

# Table d'association pour une relation de plusieurs à plusieurs entre Article et Tag
association_article_tag = Table('association_article_tag', Base.metadata,
    Column('article_id', Integer, ForeignKey('article.id_article')),
    Column('tag_id', Integer, ForeignKey('tag.id_tag'))
)

# Définir les entités
class Depeche(Base):
    __tablename__ = 'depeche'
    id_depeche = Column(Integer, primary_key=True)
    titre = Column(String)
    contenu = Column(String)
    date_publication = Column(String)
    source = Column(String)
    articles = relationship("Article", back_populates="depeche")

class Article(Base):
    __tablename__ = 'article'
    id_article = Column(Integer, primary_key=True)
    titre = Column(String)
    contenu = Column(String)
    date_creation = Column(String)
    url = Column(String)
    statut = Column(String)
    auteur = Column(String)
    id_depeche = Column(Integer, ForeignKey('depeche.id_depeche'))
    depeche = relationship("Depeche", back_populates="articles")
    illustrations = relationship("Illustration", back_populates="article")
    tags = relationship("Tag", secondary=association_article_tag, back_populates="articles")

class Illustration(Base):
    __tablename__ = 'illustration'
    id_illustration = Column(Integer, primary_key=True)
    url = Column(String)
    description = Column(String)
    id_article = Column(Integer, ForeignKey('article.id_article'))
    article = relationship("Article", back_populates="illustrations")

class Tag(Base):
    __tablename__ = 'tag'
    id_tag = Column(Integer, primary_key=True)
    nom = Column(String)
    articles = relationship("Article", secondary=association_article_tag, back_populates="tags")

# Créer un moteur et une session
engine = create_engine('sqlite:///blog_info_auto.db')
Base.metadata.create_all(engine)

# Générer le diagramme UML avec eralchemy
from eralchemy import render_er
render_er('sqlite:///blog_info_auto.db', 'diagram.png')
