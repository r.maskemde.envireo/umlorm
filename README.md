          Travaux pratique UML ORM


          A) Blog information automatique

          1) Lister les données nécessaires
   
            	Depêche AFP
                    •	id
                    •	titre
                    •	contenu
                    •	date de création
                Article
                    •	id
                    •	titre
                    •	contenu
                    •	date de création
                    •	id_depêche_afp (relation avec la dépêche AFP)
                Illustration
                    •	id
                    •	url
                    •	description
                    •	id_article (relation avec l'article)
                Tag
                    •	id
                    •	nom
                ArticleTag
                    •	id_article
                    •	id_tag
       
       2) créer le diagramme de classe UML
   
   @startuml
class "Depêche AFP" {
    - id: int
    - titre: string
    - contenu: string
    - dateCreation: date
}

class Article {
    - id: int
    - titre: string
    - contenu: string
    - dateCreation: date
    - idDepêcheAFP: int
}

class Illustration {
    - id: int
    - url: string
    - description: string
    - idArticle: int
}

class Tag {
    - id: int
    - nom: string
}

class ArticleTag {
    - idArticle: int
    - idTag: int
}

"Depêche AFP" --> Article : contient >
Article --> Illustration : illustré par >
Article --> ArticleTag : lié à >
Tag --> ArticleTag : lié à >
@enduml


       3)  créer le diagramme MCD Merise
   
        Depêche: id, titre, contenu, date_creation
        Article: id, titre, contenu, date_creation
        Illustration: id, url, description
        Tag: id, nom

        Depêche, Article, association_generer: id_depêche, id_article
        Article, Illustration, association_illustrer: id_article, id_illustration
        Article, Tag, association_tagger: id_article, id_tag

            https://www.mocodo.net/?mcd=eNpljkEOgjAQRfdzijlAF7JlV8uITSohUNcEaY0kCKS0l_BaXsyKxpC4-_Pn5f-fUfl8iCM1_FCm2F7nJoTeMOym0dvRMzStt8ArLYWiFH3vB7v5LlNw3dtwNnKm4dELs_npOVyGfrmtF2iepzi2dwvyxPOYFtwAkFNBFdfEcFfgt4hhkmC2mQYZ1aKS-w8Vg7Y0SKXOtf4PiXptegE0-kTh

      1) Création du schéma relationnel avec un ORM avec le code Python avec SQLAlchemy

        from sqlalchemy import create_engine, Column, Integer, String, ForeignKey, Table
        from sqlalchemy.ext.declarative import declarative_base
        from sqlalchemy.orm import relationship, sessionmaker

        # Définir la base déclarative
        Base = declarative_base()

        # Table d'association pour une relation de plusieurs à plusieurs entre Article et Tag
        association_article_tag = Table('association_article_tag', Base.metadata,
            Column('article_id', Integer, ForeignKey('article.id')),
            Column('tag_id', Integer, ForeignKey('tag.id'))
        )

        class DepecheAFP(Base):
            __tablename__ = 'depeche_afp'
            id = Column(Integer, primary_key=True)
            afp_uuid = Column(String, unique=True)
            contenu = Column(String)
            date = Column(String)
            articles = relationship("Article", back_populates="depeche_afp")

        class Article(Base):
            __tablename__ = 'article'
            id = Column(Integer, primary_key=True)
            titre = Column(String)
            contenu = Column(String)
            source = Column(String)
            créé_le = Column(String)
            mis_à_jour_le = Column(String)
            publié_le = Column(String)
            depeche_afp_id = Column(Integer, ForeignKey('depeche_afp.id'))
            depeche_afp = relationship("DepecheAFP", back_populates="articles")
            illustrations = relationship("Illustration", back_populates="article")
            tags = relationship("Tag", secondary=association_article_tag, back_populates="articles")

        class Illustration(Base):
            __tablename__ = 'illustration'
            id = Column(Integer, primary_key=True)
            url = Column(String)
            description = Column(String)
            article_id = Column(Integer, ForeignKey('article.id'))
            article = relationship("Article", back_populates="illustrations")

        class Tag(Base):
            __tablename__ = 'tag'
            id = Column(Integer, primary_key=True)
            nom = Column(String, unique=True)
            articles = relationship("Article", secondary=association_article_tag, back_populates="tags")

        # Créer un moteur et une session
        engine = create_engine('sqlite:///blog_info_auto.db')
        Base.metadata.create_all(engine)
        Session = sessionmaker(bind=engine)
        session = Session()

        # Exemple d'ajout de données
        depeche = DepecheAFP(afp_uuid='12345', contenu='Contenu de la dépêche', date='2024-05-24')
        article = Article(titre='Titre de l\'article', contenu='Contenu de l\'article', source='AFP', créé_le='2024-05-24', mis_à_jour_le='2024-05-24', publié_le='2024-05-24', depeche_afp=depeche)
        tag = Tag(nom='Actualités')
        article.tags.append(tag)
        illustration = Illustration(url='http://example.com/image.png', description='Une description de l\'image', article=article)

        session.add(depeche)
        session.add(article)
        session.add(tag)
        session.add(illustration)
        session.commit()
        


        B) Gestion hôtelière

        1. Représenter le diagramme des cas d’utilisation de ce système
@startuml
left to right direction
set separator none

actor "Administrateur de l'hôtel" as ad
actor "Agence de Réservation ou Client" as ag
actor "Personnel de l'hôtel" as p

rectangle "Gestion hôtel" {
    usecase "Login" as L
    usecase "Maintenance des hôtels" as M1
    usecase "Gestion d'un hôtel" as M2
    usecase "Gestion des chambres" as M3
    usecase "Consultation des réservations" as C1
    usecase "Consultation des disponibilités" as C2
    usecase "Enregistrement des réservations" as E1
    usecase "Enregistrement des arrhes" as E2
    usecase "Enregistrement des arrivées" as E3
    usecase "Gestion des consommations" as G1
    usecase "Etablissement des factures" as F1
    usecase "Edition de la liste des arrivées" as E4
    usecase "Etat d'occupation des chambres" as O1
}

ad --> L
ad --> M1
ad --> M2
ad --> M3
ad --> C1
ad --> C2
ad --> E1
ad --> E2
ad --> E3
ad --> G1
ad --> F1
ad --> E4
ad --> O1

ag --> C2
ag --> E1
ag --> E2

p --> E3
p --> G1
p --> F1
p --> E4
p --> O1
@enduml


            2. Réalisez le diagramme de classes pour modéliser les éléments ci-dessus, en
                définissant les attributs et les méthodes (opérations) de chaque classe de ce
                digramme, ainsi que les cardinalités des associations entre les classes.

                    Classe Hotel

                    Attributs :
                    id: int
                    nom: string
                    adresse: string
                    classification: int (1 à 4 étoiles)

                    Méthodes :
                    addChambre(chambre: Chambre)
                    addReservation(reservation: Reservation)
                    addFacture(facture: Facture)

                    Classe Chambre

                    Attributs :
                    id: int
                    numero: int
                    categorie: string (9 catégories possibles)
                    capacite: int
                    confort: string
                    tarif: float

                    Méthodes :
                    assignerHotel(hotel: Hotel)

                    Classe Client

                    Attributs :
                    id: int
                    nom: string
                    adresse: string
                    telephone: string
                    email: string

                    Méthodes :
                    effectuerReservation(reservation: Reservation)

                    Classe Reservation

                    Attributs :
                    id: int
                    code: string
                    dateReservation: date
                    dateDebut: date
                    dateFin: date
                    montantTotal: float
                    arrhes: float

                    Méthodes :
                    confirmer()
                    annuler()

                    Classe Facture

                    Attributs :
                    id: int
                    dateEmission: date
                    montantTotal: float

                    Méthodes :
                    ajouterPrestation(prestation: Prestation)
                    Classe Prestation

                    Attributs :
                    id: int
                    description: string
                    prix: float

                    Relations entre les Classes
                    Hotel contient Chambre (1 à N)
                    Hotel gère Reservation (1 à N)
                    Hotel émet Facture (1 à N)
                    Chambre est assignée à Hotel (N à 1)
                    Reservation est faite par Client (N à 1)
                    Reservation concerne Chambre (N à 1)
                    Reservation concerne Hotel (N à 1)
                    Facture est liée à Reservation (N à 1)
                    Facture inclut Prestation (1 à N)


 @startuml
class Hotel {
    +int id
    +string nom
    +string adresse
    +int classification
    +addChambre(Chambre chambre)
    +addReservation(Reservation reservation)
    +addFacture(Facture facture)
}

class Chambre {
    +int id
    +int numero
    +string categorie
    +int capacite
    +string confort
    +float tarif
    +assignerHotel(Hotel hotel)
}

class Client {
    +int id
    +string nom
    +string adresse
    +string telephone
    +string email
    +effectuerReservation(Reservation reservation)
}

class Reservation {
    +int id
    +string code
    +date dateReservation
    +date dateDebut
    +date dateFin
    +float montantTotal
    +float arrhes
    +confirmer()
    +annuler()
}

class Facture {
    +int id
    +date dateEmission
    +float montantTotal
    +ajouterPrestation(Prestation prestation)
}

class Prestation {
    +int id
    +string description
    +float prix
}

Hotel "1" -- "0..*" Chambre : contient >
Hotel "1" -- "0..*" Reservation : gère >
Hotel "1" -- "0..*" Facture : émet >
Chambre "0..*" -- "1" Hotel : est assignée à >
Reservation "0..*" -- "1" Client : est faite par >
Reservation "0..*" -- "1" Chambre : concerne >
Reservation "0..*" -- "1" Hotel : concerne >
Facture "0..*" -- "1" Reservation : est liée à >
Facture "1" -- "0..*" Prestation : inclut >
@enduml


3. Réalisez le diagramme d’activité du processus de réservation
   
   @startuml
start

:Client remplit le formulaire de réservation;
:Vérification de la disponibilité;

if (Chambres disponibles?) then (oui)
    :Créer la réservation;
    :Calculer le montant total et les arrhes;
    :Enregistrer la réservation;
    :Confirmer la réservation au client;
    else (non)
    :Informer le client de l'indisponibilité;
endif

stop
@enduml


4. Réalisez le diagramme de séquence du processus de réservation
   
   @startuml
actor Client
participant "Système de Réservation" as System
participant "Hôtel" as Hotel

Client -> System: Remplir le formulaire de réservation
System -> Hotel: Vérifier la disponibilité
Hotel --> System: Disponibilité confirmée
System -> Client: Confirmer la disponibilité
Client -> System: Confirmer la réservation
System -> Hotel: Enregistrer la réservation
Hotel --> System: Réservation enregistrée
System -> Client: Confirmation de la réservation
@enduml
